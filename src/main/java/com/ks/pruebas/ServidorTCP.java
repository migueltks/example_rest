package com.ks.pruebas;

import com.ks.tcp.*;

import java.util.Hashtable;

/**
 * Created by Miguel on 25/10/2016.
 */
public class ServidorTCP extends Servidor implements EventosTCP
{
    // Crear una hash de respuesta por objeto
    private static Hashtable<String, String> VMhasRespuestas;
    private static Respuesta[] VMobjRespuestas;

    public void conexionEstablecida(Cliente cliente)
    {

    }

    public void errorConexion(String s)
    {

    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp)
    {
        //PRoceso mi respuesta
        String VLstrDatos[] = s.split(Comunicaciones.FS);
        VMhasRespuestas.put(VLstrDatos[0], VLstrDatos[1]);
    }

    public void cerrarConexion(Cliente cliente)
    {

    }

    public static boolean isResponse(String componente)
    {
        return VMhasRespuestas.containsKey(componente);
    }

    public static String getResponse(String componente)
    {
        String VLstrDatos = VMhasRespuestas.get(componente);
        synchronized (VMhasRespuestas)
        {
            VMhasRespuestas.remove(componente);
        }
        return VLstrDatos;
    }

    public static Respuesta[] getResponses()
    {
        Respuesta VLobjRespuesta[] = VMobjRespuestas;
        VMobjRespuestas = null;
        return VLobjRespuesta;
    }
}
