package com.ks.pruebas;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Miguel on 25/10/2016.
 */

@RestController
public class ControllerComponents
{
    @RequestMapping("/iniciarClase")
    public Respuesta iniciar(@RequestParam(value = "componente") Funcionalidad valor)
    {

        return new Respuesta();
    }

    @RequestMapping("/iniciar")
    public Respuesta iniciar2(@RequestParam(value = "componente", defaultValue = "") String componente,
                                  @RequestParam(value = "config", defaultValue = "") String config)
    {
        Respuesta VLobjRespuesta = new Respuesta();
        Funcionalidad VLobjClase = new Funcionalidad();
        VLobjClase.iniciar(componente, config);

        VLobjRespuesta.setComponente(componente);
        VLobjRespuesta.setConfiguracion(config);

        //// while hasta que tenga la respuesta
        while (!ServidorTCP.isResponse(componente))
        {
            try
            {
                Thread.sleep(100);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }

        VLobjRespuesta.setEstado(ServidorTCP.getResponse(componente));

        return VLobjRespuesta;
    }

    @RequestMapping("/iniciarTodos")
    public Respuesta[] iniciarTodos()
    {

        // for cada correo
        for (int i=0 ; i < 5; i++)
        {

            Funcionalidad VLobjClase = new Funcionalidad();
            VLobjClase.iniciar(componente, config);
        }
        // for cada router
        for (int i=0 ; i < 5; i++)
        {

            Funcionalidad VLobjClase = new Funcionalidad();
            VLobjClase.iniciar(componente, config);
        }

        // for cada XREF
        for (int i=0 ; i < 5; i++)
        {

            Funcionalidad VLobjClase = new Funcionalidad();
            VLobjClase.iniciar(componente, config);
        }




        //// while hasta que tenga la respuesta
        while (!ServidorTCP.isResponse(componente))
        {
            try
            {
                Thread.sleep(100);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }


        return ServidorTCP.getResponses();
    }

}
